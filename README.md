maXbox
======

****************************************************************
Release Notes maXbox 3.9.9.180 Feb 2015 CODEsign
****************************************************************
Add 16 Units, 1 Slide,Tutor, Big Numbers (Decimals, TInteger)

1065 unit uPSI_UDict;                          //DFF
1066 unit uPSI_ubigFloatV3;                    //DFF
1067 unit uPSI_UBigIntsV4;                     //DFF 
1068 unit uPSI_ServiceMgr2;                    //mX
1069 unit uPSI_UP10Build;                      //PS
1070 unit uPSI_UParser10;                      //PS
1071 unit uPSI_IdModBusServer;                 //MB
1072 unit uPSI_IdModBusClient; +MBUtils        //MB
1073 unit uPSI_ColorGrd;                       //VCL
1074 unit uPSI_DirOutln;                       //VCL
1075 unit uPSI_Gauges;                         //VCL
1076 unit uPSI_CustomizeDlg;                   //VCL
1077 unit uPSI_ActnMan;                        //VCL
1078 unit uPSI_CollPanl;                       //VCL
1079 unit uPSI_Calendar2;                      //VCL
1080 unit uPSI_IBCtrls;                        //VCL

SHA1:  maXbox3.exe 680C38764AABF79B953713CDE1501B77BE27FA49
CRC32: maXbox3.exe 5878C1C3
****************************************************************
Release Notes maXbox 3.9.9.88 March 2014
****************************************************************
2 Tutorials 30 Units add, VCL constructors, controls+, unit list

786 uPSI_FileUtil;
787 uPSI_changefind;
788 uPSI_cmdIntf;
789 uPSI_fservice;
790 uPSI_Keyboard;
791 uPSI_VRMLParser,
792 uPSI_GLFileVRML,
793 uPSI_Octree;
794 uPSI_GLPolyhedron,
795 uPSI_GLCrossPlatform;
796 uPSI_GLParticles;
797 uPSI_GLNavigator;
798 uPSI_GLStarRecord;
799 uPSI_TGA;
800 uPSI_GLCanvas;
801 uPSI_GeometryBB;
802 uPSI_GeometryCoordinates;
803 uPSI_VectorGeometry;
804 uPSI_BumpMapping;
805 uPSI_GLTextureCombiners;
806 uPSI_GLVectorFileObjects;
807 uPSI_IMM;
808 uPSI_CategoryButtons;
809 uPSI_ButtonGroup;
810 uPSI_DbExcept;
811 uPSI_AxCtrls;
812 uPSI_GL_actorUnit1;
813 uPSI_StdVCL;
814 unit CurvesAndSurfaces;
815 uPSI_DataAwareMain;

SHA1: Win 3.9.9.88: 119533C0725A9B9B2919849759AA2F6298EBFF28